package com.mobimedia.vodacom.content.delivery.composer;

import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_WEATHER_BLOEMFONTEIN;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_WEATHER_CAPE_TOWN;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_WEATHER_DURBAN;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_WEATHER_JOHANNESBURG;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_WEATHER_PORT_ELIZABETH;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_WEATHER_PRETORIA;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_WEATHER_BLOEMFONTEIN;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_WEATHER_CAPE_TOWN;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_WEATHER_DURBAN;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_WEATHER_JOHANNESBURG;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_WEATHER_PORT_ELIZABETH;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_WEATHER_PRETORIA;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.mobimedia.vodacom.content.delivery.config.AppConfig;
import com.mobimedia.vodacom.content.delivery.contenttype.Weather;
import com.mobimedia.vodacom.content.delivery.entities.SubmitTaskItem;
import com.mobimedia.vodacom.content.delivery.utils.HibernateUtil;

public class WeatherContentComposer {
	private static final Logger log = Logger.getLogger(WeatherContentComposer.class);
	
	private Weather weather;
	private Date date;
	
	public WeatherContentComposer(Weather weather, Date date) {
		this.weather = weather;
		this.date = date;
	}
	
	@SuppressWarnings("unchecked")
	public SubmitTaskItem getSubmitTaskItem() {
		List<String> recipients = new ArrayList<String>();
		String serviceId = null;
		String message = null;
		String fusionSubName = null;
		
		Session session = null;
		SubmitTaskItem item = null;
		
		try {
        	session = HibernateUtil.getSessionFactory().openSession();
		
			switch (weather) {
				case BLOEMFONTEIN:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_sa_weather AS s WHERE channel = :channel")
			        		.setParameter("channel", "Bloemfontein")
			        		.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_WEATHER_BLOEMFONTEIN)
			        		.list();
			        if (textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (!AppConfig.getInstance().skipDupicateContentCheck() && logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Bloemfontein Weather message send detected, skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_weat AS s WHERE stype = :stype")
							.setParameter("stype", "Bloemfontein")
							.list();
			        
			        log.info("Weather Bloemfontein recipients = " + recipients.size());
					
			        serviceId = SERVICEID_WEATHER_BLOEMFONTEIN;
			        fusionSubName = FUSION_SUBNAME_WEATHER_BLOEMFONTEIN;
				}
				break;
				
				case CAPE_TOWN:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_sa_weather AS s WHERE channel = :channel")
			        		.setParameter("channel", "Cape Town")
			        		.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_WEATHER_CAPE_TOWN)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Cape Town Weather message send detected, skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_weat AS s WHERE stype = :stype")
							.setParameter("stype", "Cape Town")
							.list();
			        
			        log.info("Weather Cape Town recipients = " + recipients.size());
					
			        serviceId = SERVICEID_WEATHER_CAPE_TOWN;
			        fusionSubName = FUSION_SUBNAME_WEATHER_CAPE_TOWN;
				}
				break;
				
				case DURBAN:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_sa_weather AS s WHERE channel = :channel")
			        		.setParameter("channel", "Durban")
			        		.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_WEATHER_DURBAN)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Durban Weather message send detected, skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_weat AS s WHERE stype = :stype")
							.setParameter("stype", "Durban")
							.list();
			        
			        log.info("Weather Durban recipients = " + recipients.size());
					
			        serviceId = SERVICEID_WEATHER_DURBAN;
			        fusionSubName = FUSION_SUBNAME_WEATHER_DURBAN;
				}
				break;
				
				case JOHANNESBURG:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_sa_weather AS s WHERE channel = :channel")
			        		.setParameter("channel", "Johannesburg")
			        		.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_WEATHER_JOHANNESBURG)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Johannesburg Weather message send detected, skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_weat AS s WHERE stype = :stype")
							.setParameter("stype", "Johannesburg")
							.list();
			        
			        log.info("Weather Johannesburg recipients = " + recipients.size());
					
			        serviceId = SERVICEID_WEATHER_JOHANNESBURG;
			        fusionSubName = FUSION_SUBNAME_WEATHER_JOHANNESBURG;
				}
				break;
				
				case PRETORIA:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_sa_weather AS s WHERE channel = :channel")
			        		.setParameter("channel", "Pretoria")
			        		.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_WEATHER_PRETORIA)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Pretoria Weather message send detected, skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_weat AS s WHERE stype = :stype")
							.setParameter("stype", "Pretoria")
							.list();
			        
			        log.info("Weather Pretoria recipients = " + recipients.size());
					
			        serviceId = SERVICEID_WEATHER_PRETORIA;
			        fusionSubName = FUSION_SUBNAME_WEATHER_PRETORIA;
				}
				break;
				
				case PORT_ELIZABETH:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_sa_weather AS s WHERE channel = :channel")
			        		.setParameter("channel", "Port Elizabeth")
			        		.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_WEATHER_PORT_ELIZABETH)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Port Elizabeth Weather message send detected, skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_weat AS s WHERE stype = :stype")
							.setParameter("stype", "Port Elizabeth")
							.list();
			        
			        log.info("Weather Port Elizabeth recipients = " + recipients.size());
					
			        serviceId = SERVICEID_WEATHER_PORT_ELIZABETH;
			        fusionSubName = FUSION_SUBNAME_WEATHER_PORT_ELIZABETH;
				}
				break;
					
				default:
					throw new RuntimeException("Unable to determine content type to send");
			}
			
			item = new SubmitTaskItem(recipients, serviceId, message, fusionSubName);
		} catch (Exception e) {
			// TODO determine which exception and handle appropriately (error queue for retry?)
			log.error(e);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {}
			}
			session = null;
		}
		
		return item;
	}

}
