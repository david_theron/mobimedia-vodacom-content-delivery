package com.mobimedia.vodacom.content.delivery;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Defines the components of a JAX-RS application to be injected into 
 * resource classes and providers.
 * 
 * @author craignewton <newtondev@gmail.com>
 * @see http://docs.oracle.com/javaee/6/api/javax/ws/rs/core/Application.html
 */
@ApplicationPath("content")
public class WebServiceApplication extends Application {
	/* 
	 * Nothing to do here as we are just subclassing
	 * and inheriting all the methods from the parent. 
	 */	
}