package com.mobimedia.vodacom.content.delivery.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="subscriber_weat")
public class SubscriberWeather {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String msisdn;
	
	private String sid;
	
	private String stype;
	
	private String pid;
	
	private String time;
	
	public SubscriberWeather() {
		
	}
	
	public SubscriberWeather(Long id, String msisdn, String sid, String stype, String pid, String time) {
		this.id = id;
		this.msisdn = msisdn;
		this.sid = sid;
		this.stype = stype;
		this.pid = pid;
		this.time = time;
	}
	
	@Column(name="id")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="msisdn")
	public String getMsisdn() {
		return msisdn;
	}
	
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	
	@Column(name="sid")
	public String getSid() {
		return sid;
	}
	
	public void setSid(String sid) {
		this.sid = sid;
	}
	
	@Column(name="stype")
	public String getStype() {
		return stype;
	}
	
	public void setStype(String stype) {
		this.stype = stype;
	}

	@Column(name="pid")
	public String getPid() {
		return pid;
	}
	
	public void setPid(String pid) {
		this.pid = pid;
	}
	
	@Column(name="time")
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
}