package com.mobimedia.vodacom.content.delivery.rs.zangoma;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mobimedia.vodacom.content.delivery.contenttype.Zangoma;
import com.mobimedia.vodacom.content.delivery.managers.ContentComposerManager;
import com.mobimedia.vodacom.content.delivery.utils.AuthorizationUtil;

@Path("/zang/all")
public class AllZangomaRestService {
	private static final Logger log = Logger.getLogger(AllZangomaRestService.class);
	
	@Produces({ "application/json" })
	@GET
	public Response triggerSend(@Context HttpServletRequest request) {
		// Make sure only authorized access is allowed
		if (!AuthorizationUtil.isAuthorized(request)) {
			return Response.status(401).build();
		}
		
		Thread th = new Thread(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendZangoma(Zangoma.ENGLISH, new Date());
				ContentComposerManager.sendZangoma(Zangoma.ZULU, new Date());
			}
		});
		th.start();
		
		log.info("Web Service initiating Zangoma send for All");
		
		return Response.accepted().build();
	}
}