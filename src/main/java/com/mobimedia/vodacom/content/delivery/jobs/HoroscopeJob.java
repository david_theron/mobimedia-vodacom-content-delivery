package com.mobimedia.vodacom.content.delivery.jobs;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.mobimedia.vodacom.content.delivery.managers.TaskExecutionManager;

public class HoroscopeJob implements Job {
	private static final Logger log = Logger.getLogger(HoroscopeJob.class);
	
	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		log.info("Starting Horoscope Scheduled Job");
		
		TaskExecutionManager.getInstance().executeHoroscopeSend();
		
		log.info("Finished Horoscope Scheduled Job");
	}
}
