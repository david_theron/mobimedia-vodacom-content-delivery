package com.mobimedia.vodacom.content.delivery.rs.error;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.mobimedia.vodacom.content.delivery.managers.TaskExecutionManager;
import com.mobimedia.vodacom.content.delivery.utils.AuthorizationUtil;

@Path("/error/fusionlog/reinject")
public class FusionLogErrorReinjectRestService {
	
	@Produces({ "application/json" })
	@GET
	public Response reinject(@Context HttpServletRequest request) {
		// Make sure only authorized access is allowed
		if (!AuthorizationUtil.isAuthorized(request)) {
			return Response.status(401).build();
		}
				
		TaskExecutionManager.getInstance().executeErrorFusionLogReinject();
		
		return Response.accepted().build();
	}
}