package com.mobimedia.vodacom.content.delivery.composer;

import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_ZANGOMA_ENGLISH;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_ZANGOMA_ZULU;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_ZANGOMA_ENGLISH;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_ZANGOMA_ZULU;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.mobimedia.vodacom.content.delivery.contenttype.Zangoma;
import com.mobimedia.vodacom.content.delivery.entities.SubmitTaskItem;
import com.mobimedia.vodacom.content.delivery.utils.HibernateUtil;

public class ZangomaContentComposer {
	private static final Logger log = Logger.getLogger(ZangomaContentComposer.class);
	
	private Zangoma zangoma;
	private Date date;
	
	public ZangomaContentComposer(Zangoma zangoma, Date date) {
		this.zangoma = zangoma;
		this.date = date;
	}
	
	@SuppressWarnings("unchecked")
	public SubmitTaskItem getSubmitTaskItem() {
		List<String> recipients = new ArrayList<String>();
		String serviceId = null;
		String message = null;
		String fusionSubName = null;
		
		Session session = null;
		SubmitTaskItem item = null;
		
		try {
        	session = HibernateUtil.getSessionFactory().openSession();
		
			switch (zangoma) {
				case ENGLISH:
				{
					Random random = new Random(System.currentTimeMillis());
					int id = random.nextInt(199)+1;
					
					List<String> text = session.createSQLQuery("Select s.text FROM sms_zangoma AS s WHERE num = :num")
			        		.setParameter("num", id)
			        		.list();
					
			        message = text.get(0);
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_zang AS s WHERE stype = :stype")
							.setParameter("stype", "English")
							.list();
					
			        serviceId = SERVICEID_ZANGOMA_ENGLISH;
			        fusionSubName = FUSION_SUBNAME_ZANGOMA_ENGLISH;
				}
				break;
				
				case ZULU:
				{
					Random random = new Random(System.currentTimeMillis());
					int id = random.nextInt(199)+1;
					
					List<String> text = session.createSQLQuery("Select s.text FROM sms_zangoma_zul AS s WHERE num = :num")
			        		.setParameter("num", id)
			        		.list();
					
			        message = text.get(0);
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_zang AS s WHERE stype = :stype")
							.setParameter("stype", "Zulu")
							.list();
					
			        serviceId = SERVICEID_ZANGOMA_ZULU;
			        fusionSubName = FUSION_SUBNAME_ZANGOMA_ZULU;
				}
				break;
					
				default:
					throw new RuntimeException("Unable to determine content type to send");
			}
			
			item = new SubmitTaskItem(recipients, serviceId, message, fusionSubName);
		} catch (Exception e) {
			// TODO determine which exception and handle appropriately (error queue for retry?)
			log.error(e);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {}
			}
			session = null;
		}
		
		return item;
	}

}
