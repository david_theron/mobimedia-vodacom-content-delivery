package com.mobimedia.vodacom.content.delivery.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.google.common.collect.Lists;
import com.mobimedia.vodacom.content.delivery.config.AppConfig;
import com.mobimedia.vodacom.content.delivery.entities.SubmitTaskItem;

public class SendManager {
	
	private static final Logger log = Logger.getLogger(SendManager.class);
	
	private static SendManager instance = null;
	
	private ExecutorService executorService = null;
	
	private final Queue<BatchSendErrorItem> errorQueue = 
			new ConcurrentLinkedQueue<BatchSendErrorItem>();
	
	private SendManager() {
		executorService = new ThreadPoolExecutor(
				AppConfig.getInstance().getMaxFusionConcurrentConnections(), 
				AppConfig.getInstance().getMaxFusionConcurrentConnections(), 
				5000, TimeUnit.MILLISECONDS, 
				new LinkedBlockingQueue<Runnable>());
	}
	
	public List<BatchSendErrorItem> getErrorsAsList() {
		List<BatchSendErrorItem> errors = new ArrayList<BatchSendErrorItem>();
		Object[] errorsArray = errorQueue.toArray();
		for (int i=0; i < errorsArray.length; i++) {
			errors.add((BatchSendErrorItem)errorsArray[i]);
		}
		return errors;
	}
	
	public static SendManager getInstance() {
		if (instance == null) {
			instance = new SendManager();
		}
		return instance;
	}

	public void queueSendItem(SubmitTaskItem item) {
		log.info("Outgoing message: " + item.getMessage());
		
		int batchNum = 1;
		if (AppConfig.getInstance().isBatchSendEnabled()) {
			// Send in batches
			for (List<String> msisdns : Lists.partition(item.getRecipients(), AppConfig.getInstance().getBatchSendSize())) {
				BatchSendTask batchSendTask = new BatchSendTask(msisdns, item.getServiceId(), 
						item.getMessage(), item.getFusionSubName(), batchNum);
				try {
					executorService.execute(new BatchSendTask(msisdns, item.getServiceId(), 
						item.getMessage(), item.getFusionSubName(), batchNum));
				} catch (Exception e) {
					log.error(e);
					addToErrorQueue(batchSendTask, e);
				}
				
				batchNum++;
			}
		} else {
			for (String msisdn : item.getRecipients()) {
				List<String> msisdns = new ArrayList<String>();
				msisdns.add(msisdn);
				BatchSendTask batchSendTask = new BatchSendTask(msisdns, item.getServiceId(), 
						item.getMessage(), item.getFusionSubName(), batchNum);
				try {
					executorService.execute(new BatchSendTask(msisdns, item.getServiceId(), 
						item.getMessage(), item.getFusionSubName(), batchNum));
				} catch (Exception e) {
					log.error(e);
					addToErrorQueue(batchSendTask, e);
				}
				
				batchNum++;
			}
		}
	}
	
	public void addToErrorQueue(BatchSendTask batchSendTask, Exception e) {
		errorQueue.add(new BatchSendErrorItem(batchSendTask, e));
	}
	
	public class BatchSendErrorItem {
		private BatchSendTask task;
		private Exception exception;
		
		public BatchSendErrorItem(BatchSendTask task, Exception exception) {
			this.task = task;
			this.exception = exception;
		}
		
		public BatchSendTask getTask() {
			return task;
		}
		
		public Exception getException() {
			return exception;
		}
	}

	public void reinjectErrors() {
		while (!errorQueue.isEmpty()) {
			BatchSendErrorItem item = errorQueue.poll();
			if (item != null) {
				BatchSendTask task = item.getTask();
				if (task != null) {
					try {
						executorService.execute(task);
					} catch (Exception e) {
						log.error(e);
						addToErrorQueue(task, e);
					}
				}
			}
			try {
				Thread.sleep(100);
			} catch (Exception e) {}
		}
	}
	
	public int getNumberOfActiveThreads() {
		int active = 0;
		if (executorService != null) {
			active = ((ThreadPoolExecutor)executorService).getActiveCount();
		}
		return active;
	}
}
