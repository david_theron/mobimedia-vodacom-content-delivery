package com.mobimedia.vodacom.content.delivery.jobs;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.mobimedia.vodacom.content.delivery.managers.TaskExecutionManager;

public class WeatherJob implements Job {
	private static final Logger log = Logger.getLogger(WeatherJob.class);
	
	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		log.info("Starting Weather Scheduled Job");

		TaskExecutionManager.getInstance().executeWeatherSend();
		
		log.info("Finished Weather Scheduled Job");
	}
}
