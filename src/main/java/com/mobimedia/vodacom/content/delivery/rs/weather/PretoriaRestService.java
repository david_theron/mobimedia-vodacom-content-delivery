package com.mobimedia.vodacom.content.delivery.rs.weather;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mobimedia.vodacom.content.delivery.managers.TaskExecutionManager;
import com.mobimedia.vodacom.content.delivery.utils.AuthorizationUtil;

@Path("/weat/pta")
public class PretoriaRestService {
	private static final Logger log = Logger.getLogger(PretoriaRestService.class);
	
	@Produces({ "application/json" })
	@GET
	public Response triggerSend(@Context HttpServletRequest request) {
		// Make sure only authorized access is allowed
		if (!AuthorizationUtil.isAuthorized(request)) {
			return Response.status(401).build();
		}
		
		TaskExecutionManager.getInstance().executeWeatherPretoriaSend();
		
		log.info("Web Service initiating Weather send for Pretoria");
		
		return Response.accepted().build();
	}
}