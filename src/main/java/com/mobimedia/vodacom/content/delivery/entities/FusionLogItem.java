package com.mobimedia.vodacom.content.delivery.entities;

import java.sql.Timestamp;

public class FusionLogItem {
	
	private String response;
	private String msisdns;
	private String subName;
	private String transactionId;
	private int totalSubscribers;
	private String message;
	private Timestamp timestamp;
	private Long responseTime;
	private boolean isSport = false;

	public FusionLogItem(String response, String msisdns, String subName, 
			String transactionId, int totalSubscribers, String message, 
			Timestamp timestamp, Long responseTime, boolean isSport) {
		this.response = response;
		this.msisdns = msisdns;
		this.subName = subName;
		this.transactionId = transactionId;
		this.totalSubscribers = totalSubscribers;
		this.message = message;
		this.timestamp = timestamp;
		this.responseTime = responseTime;
		this.isSport = isSport;
	}
	
	public String getResponse() {
		return response;
	}
	
	public String getMsisdns() {
		return msisdns;
	}
	
	public String getSubName() {
		return subName;
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	
	public int getTotalSubscribers() {
		return totalSubscribers;
	}
	
	public String getMessage() {
		return message;
	}
	
	public Timestamp getTimestamp() {
		return timestamp;
	}
	
	public Long getResponseTime() {
		return responseTime;
	}
	
	public boolean isSport() {
		return isSport;
	}
}
