package com.mobimedia.vodacom.content.delivery.rs.error;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.axis.encoding.Base64;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.mobimedia.vodacom.content.delivery.config.AppConfig;
import com.mobimedia.vodacom.content.delivery.entities.FusionLogItem;
import com.mobimedia.vodacom.content.delivery.managers.FusionLogManager;
import com.mobimedia.vodacom.content.delivery.managers.FusionLogManager.FusionLogErrorItem;
import com.mobimedia.vodacom.content.delivery.managers.FusionLogTask;
import com.mobimedia.vodacom.content.delivery.utils.AuthorizationUtil;

@Path("/error/fusionlog")
public class FusionLogErrorRestService {
	private static final Logger log = Logger.getLogger(FusionLogErrorRestService.class);
	
	@Produces({ "application/json" })
	@GET
	public Response fusionlog(@Context HttpServletRequest request) {
		// Make sure only authorized access is allowed
		if (!AuthorizationUtil.isAuthorized(request)) {
			return Response.status(401).build();
		}
		
		List<FusionLogErrorItem> errors = FusionLogManager.getInstance().getErrorsAsList();
		
		Gson gson = new Gson();
		List<JsonResponseItem> responseItems = new ArrayList<JsonResponseItem>();
		for (FusionLogErrorItem item : errors) {
			responseItems.add(new JsonResponseItem(item));
		}
		String response = gson.toJson(responseItems);
		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}
	
	public class JsonResponseItem {
		
		private String exceptionType;
		private String exceptionMessage;
		private String transactionId;
		private String message;
		private String fusionSubName;
		
		public JsonResponseItem(FusionLogErrorItem item) {
			FusionLogTask task = item.getTask();
			if (task != null) {
				FusionLogItem fsLogItem = task.getItem();
				if (fsLogItem != null) {
					transactionId = fsLogItem.getTransactionId();
					message = fsLogItem.getMessage();
					fusionSubName = fsLogItem.getSubName();
				}
			}
			
			Exception exception = item.getException();
			if (exception != null) {
				exceptionType = exception.getClass().toString();
				exceptionMessage = exception.getMessage();
			}
		}
	}
}