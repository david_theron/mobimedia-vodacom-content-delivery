package com.mobimedia.vodacom.content.delivery.managers;

import java.util.Date;

import com.mobimedia.vodacom.content.delivery.composer.HoroscopeContentComposer;
import com.mobimedia.vodacom.content.delivery.composer.SoapieContentComposer;
import com.mobimedia.vodacom.content.delivery.composer.SportContentComposer;
import com.mobimedia.vodacom.content.delivery.composer.WeatherContentComposer;
import com.mobimedia.vodacom.content.delivery.composer.ZangomaContentComposer;
import com.mobimedia.vodacom.content.delivery.contenttype.Horoscope;
import com.mobimedia.vodacom.content.delivery.contenttype.Soapie;
import com.mobimedia.vodacom.content.delivery.contenttype.Sport;
import com.mobimedia.vodacom.content.delivery.contenttype.Weather;
import com.mobimedia.vodacom.content.delivery.contenttype.Zangoma;
import com.mobimedia.vodacom.content.delivery.entities.SubmitTaskItem;

public class ContentComposerManager {
	
	public static void sendSport(Sport sport, long id) {
		SportContentComposer composer = new SportContentComposer(sport, id);
		SubmitTaskItem item = composer.getSubmitTaskItem();
		if (item != null) {
			// Compose message object and send to SendManager
			SendManager.getInstance().queueSendItem(item);
		}
	}
	
	public static void sendHoroscope(Horoscope horoscope, Date date) {
		HoroscopeContentComposer composer = new HoroscopeContentComposer(horoscope, date);
		SubmitTaskItem item = composer.getSubmitTaskItem();
		if (item != null) {
			// Compose message object and send to SendManager
			SendManager.getInstance().queueSendItem(item);
		}
	}
	
	public static void sendSoapie(Soapie soapie, Date date) {
		SoapieContentComposer composer = new SoapieContentComposer(soapie, date);
		SubmitTaskItem item = composer.getSubmitTaskItem();
		if (item != null) {
			// Compose message object and send to SendManager
			SendManager.getInstance().queueSendItem(item);
		}
	}
	
	public static void sendWeather(Weather weather, Date date) {
		WeatherContentComposer composer = new WeatherContentComposer(weather, date);
		SubmitTaskItem item = composer.getSubmitTaskItem();
		if (item != null) {
			// Compose message object and send to SendManager
			SendManager.getInstance().queueSendItem(item);
		}
	}
	
	public static void sendZangoma(Zangoma zangoma, Date date) {
		ZangomaContentComposer composer = new ZangomaContentComposer(zangoma, date);
		SubmitTaskItem item = composer.getSubmitTaskItem();
		if (item != null) {
			// Compose message object and send to SendManager
			SendManager.getInstance().queueSendItem(item);
		}
	}
}
