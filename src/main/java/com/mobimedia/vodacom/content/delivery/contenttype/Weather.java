package com.mobimedia.vodacom.content.delivery.contenttype;

public enum Weather {
	BLOEMFONTEIN,
	CAPE_TOWN,
	DURBAN,
	JOHANNESBURG,
	PRETORIA,
	PORT_ELIZABETH
}
