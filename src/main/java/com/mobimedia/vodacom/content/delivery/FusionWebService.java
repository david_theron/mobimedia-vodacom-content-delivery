package com.mobimedia.vodacom.content.delivery;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Stub;
import org.apache.log4j.Logger;

import za.co.vodacom.sgp.NVPair;
import za.co.vodacom.sgp.SgpGenericException;
import za.co.vodacom.sgp.SgpGenericService;
import za.co.vodacom.sgp.SgpGenericServiceServiceLocator;

import com.mobimedia.vodacom.content.delivery.config.AppConfig;

public class FusionWebService {
	private static final Logger log = Logger.getLogger(FusionWebService.class);
	
	private SgpGenericServiceServiceLocator serviceLocator;
	private SgpGenericService service;
	private Stub stub;
	
	public FusionWebService() throws Exception {
		init();
	}
	
	private void init() throws ServiceException {
		serviceLocator = new SgpGenericServiceServiceLocator();
		service = serviceLocator.getGenericService();
		stub = ((Stub) service);
		stub.setTimeout(AppConfig.getInstance().getFusionTimeout());
		stub.setUsername(AppConfig.getInstance().getFusionUsername());
		stub.setPassword(AppConfig.getInstance().getFusionPassword());
	}
	
	public NVPair[] sendSmsAlert(String msisdn, String serviceId, String message, String transactionId) 
			throws SgpGenericException, RemoteException
	{
		List<String> msisdnList = new ArrayList<String>();
		msisdnList.add(msisdn);
		
		return sendSmsAlert(msisdnList, serviceId, message, transactionId);
	}
	
	public NVPair[] sendSmsAlert(List<String> msisdnList, String serviceId, String message, String transactionId) 
			throws SgpGenericException, RemoteException {
		
		StringBuilder msisdns = new StringBuilder();
		for (int i = 0; i < msisdnList.size(); i++) {
			msisdns.append(msisdnList.get(i));
			if (i < (msisdnList.size()-1)) {
				msisdns.append(",");
			}
		}
		
		NVPair[] parameters = new NVPair[3];
		NVPair msisdnParam = new NVPair("MSISDN", msisdns.toString());
		parameters[0] = msisdnParam;
		
		NVPair serviceIdParam = new NVPair("SERVICE_ID", serviceId);
		parameters[1] = serviceIdParam;
		
		NVPair messageParam = new NVPair("MESSAGE", message);
		parameters[2] = messageParam;
		
		NVPair[] result = service.execute("ALERTS", "SMSALERT", transactionId, parameters);
		try {
			String request = stub._getCall().getMessageContext().getRequestMessage().getSOAPPart().getEnvelope().getBody().toString();
			log.info("SOAP Request ("+transactionId+"): " + request);
			String response = stub._getCall().getMessageContext().getResponseMessage().getSOAPPart().getEnvelope().getBody().toString();
			log.info("SOAP Response ("+transactionId+"): " + response);
		} catch (Exception e) {
			log.error(e);
		}
		return result;
	}
}
