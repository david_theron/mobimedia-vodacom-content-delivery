package com.mobimedia.vodacom.content.delivery.config;

/**
 * Vodacom Fusion Service Id configuration for products.
 * 
 * @author craignewton
 */
public final class ServiceIdConfig {
	
	/*
	 * Sport
	 */
	public static final String SERVICEID_SPORT_EPL 				= "vc-teamt-epl-ussd-team%s-01";
	public static final String SERVICEID_SPORT_PSL				= "vc-teamt-psl-ussd-team%s-01";
	
	/*
	 * Horoscopes 
	 */
	public static final String SERVICEID_HOROSCOPE_AQUARIUS 	= "vc-teamt-horo-ussd-aqu-c-01";
	public static final String SERVICEID_HOROSCOPE_ARIES 		= "vc-teamt-horo-ussd-ari-c-01";
	public static final String SERVICEID_HOROSCOPE_CANCER 		= "vc-teamt-horo-ussd-can-c-01";
	public static final String SERVICEID_HOROSCOPE_CAPRICORN 	= "vc-teamt-horo-ussd-cap-c-01";
	public static final String SERVICEID_HOROSCOPE_GEMINI 		= "vc-teamt-horo-ussd-gem-c-01";
	public static final String SERVICEID_HOROSCOPE_LEO 			= "vc-teamt-horo-ussd-leo-c-01";
	public static final String SERVICEID_HOROSCOPE_LIBRA 		= "vc-teamt-horo-ussd-lib-c-01";
	public static final String SERVICEID_HOROSCOPE_PISCES 		= "vc-teamt-horo-ussd-pis-c-01";
	public static final String SERVICEID_HOROSCOPE_SAGITTARIUS 	= "vc-teamt-horo-ussd-sag-c-01";
	public static final String SERVICEID_HOROSCOPE_SCORPIO 		= "vc-teamt-horo-ussd-sco-c-01";
	public static final String SERVICEID_HOROSCOPE_TAURUS 		= "vc-teamt-horo-ussd-tau-c-01";
	public static final String SERVICEID_HOROSCOPE_VIRGO		= "vc-teamt-horo-ussd-vir-c-01";
	
	/*
	 * Soapies
	 */
	public static final String SERVICEID_SOAPIE_7DELAAN			= "vc-teamt-soap-ussd-7de-c-01";
	public static final String SERVICEID_SOAPIE_GENERATIONS		= "vc-teamt-soap-ussd-gen-c-01";
	public static final String SERVICEID_SOAPIE_ISIDINGO		= "vc-teamt-soap-ussd-isid-c-01";
	
	/*
	 * Weather
	 */
	public static final String SERVICEID_WEATHER_BLOEMFONTEIN	= "vc-teamt-weat-ussd-bloem-c-01";
	public static final String SERVICEID_WEATHER_CAPE_TOWN		= "vc-teamt-weat-ussd-ct-c-01";
	public static final String SERVICEID_WEATHER_DURBAN			= "vc-teamt-weat-ussd-dur-c-01";
	public static final String SERVICEID_WEATHER_JOHANNESBURG	= "vc-teamt-weat-ussd-jhb-c-01";
	public static final String SERVICEID_WEATHER_PRETORIA		= "vc-teamt-weat-ussd-pta-c-01";
	public static final String SERVICEID_WEATHER_PORT_ELIZABETH = "vc-teamt-weat-ussd-pe-c-01";
	
	/*
	 * Zangoma
	 */
	public static final String SERVICEID_ZANGOMA_ENGLISH 		= "vc-teamt-zang-ussd-eng-c-01";
	public static final String SERVICEID_ZANGOMA_ZULU			= "vc-teamt-zang-ussd-zulu-c-01";
	
}
