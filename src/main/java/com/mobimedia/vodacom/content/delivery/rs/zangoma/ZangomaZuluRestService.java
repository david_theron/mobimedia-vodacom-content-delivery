package com.mobimedia.vodacom.content.delivery.rs.zangoma;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mobimedia.vodacom.content.delivery.managers.TaskExecutionManager;
import com.mobimedia.vodacom.content.delivery.utils.AuthorizationUtil;

@Path("/zang/zul")
public class ZangomaZuluRestService {
	private static final Logger log = Logger.getLogger(ZangomaZuluRestService.class);
	
	@Produces({ "application/json" })
	@GET
	public Response triggerSend(@Context HttpServletRequest request) {
		// Make sure only authorized access is allowed
		if (!AuthorizationUtil.isAuthorized(request)) {
			return Response.status(401).build();
		}
		
		TaskExecutionManager.getInstance().executeZangomaZuluSend();
		
		log.info("Web Service initiating Zangoma send for Zulu");
		
		return Response.accepted().build();
	}
}