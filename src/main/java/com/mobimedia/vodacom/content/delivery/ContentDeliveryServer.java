package com.mobimedia.vodacom.content.delivery;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import com.mobimedia.vodacom.content.delivery.config.AppConfig;
import com.mobimedia.vodacom.content.delivery.config.WebServiceConfig;
import com.mobimedia.vodacom.content.delivery.jobs.HoroscopeJob;
import com.mobimedia.vodacom.content.delivery.jobs.SoapieJob;
import com.mobimedia.vodacom.content.delivery.jobs.WeatherJob;
import com.mobimedia.vodacom.content.delivery.jobs.ZangomaJob;
import com.mobimedia.vodacom.content.delivery.managers.FusionLogManager;
import com.mobimedia.vodacom.content.delivery.managers.SendManager;

/**
 * Vodacom USSD Content Delivery Server.
 * 
 * @author craignewton <newtondev@gmail.com>
 */
public class ContentDeliveryServer implements Daemon {
	private static final Logger log = Logger.getLogger(ContentDeliveryServer.class);
	
	private Server server = null;
	private Scheduler scheduler = null;
	private boolean initialized = false;
	private static final int PORT = 8181;
	
	private Timer reinjectSendErrorTimer;
	private Timer reinjectFusionLogErrorTimer;
	private Timer cleanupTimer;

	public static void main(final String[] args) throws Exception {
		try {
			log.info("Server starting up");
			final ContentDeliveryServer contentDeliveryServer = new ContentDeliveryServer();
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					contentDeliveryServer.shutdown();
				}
			});
			contentDeliveryServer.initServer();
			log.info("Server successfully started and listenting for requests on port: " + PORT);
		} catch (Exception e) {
			log.fatal("Fatal error starting up server", e);
			System.exit(1);
		}
	}
	
	public void initServer() throws Exception {
		if (!initialized) {
			log.info("Starting server initialization");
			
			startAdminTasks();
			scheduleJobs();
			initRestServer();
			initialized = true;
			log.info("Server successfully initialized");
		}
	}
	
	private void startAdminTasks() {
		reinjectFusionLogErrorTimer = new Timer();
		reinjectFusionLogErrorTimer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				FusionLogManager.getInstance().reinjectErrors();
			}
		}, 1000*60*10, 1000*60*10);
		
		reinjectSendErrorTimer = new Timer();
		reinjectSendErrorTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				SendManager.getInstance().reinjectErrors();
			}
		}, 1000*60*10, 1000*60*10);
		
		cleanupTimer = new Timer();
		cleanupTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				System.gc();
			}
		}, 
		1000*60*30, 1000*60*30);
	}

	private void initRestServer() throws Exception {
		// Make sure only one instance is defined and running
		if (this.server != null) {
			return;
		}
		
		// Setup the Jetty Server
		this.server = new Server(PORT);

		// Register and map the dispatcher servlet
		final ServletHolder servletHolder = new ServletHolder(new CXFServlet());
		final ServletContextHandler context = new ServletContextHandler();
		context.setContextPath("/");
		context.addServlet(servletHolder, "/*");
		context.addEventListener(new ContextLoaderListener());

		// Initialize with parameters for inital configuration
		context.setInitParameter("contextClass",
				AnnotationConfigWebApplicationContext.class.getName());
		context.setInitParameter("contextConfigLocation",
				WebServiceConfig.class.getName());

		// Set the handler and start the server
		server.setHandler(context);
		server.start();
		server.join();
	}
	
	private void scheduleJobs() {
		try {
			SchedulerFactory sf = new StdSchedulerFactory();
			scheduler = sf.getScheduler();
			
			// Start the scheduler
			scheduler.start();
			
			// Horoscopes
			JobDetail horoscopeJob = JobBuilder.newJob(HoroscopeJob.class)
					.withIdentity("horoscope_job", "content")
					.build();
			CronTrigger horoscopeTrigger = TriggerBuilder.newTrigger()
					.withIdentity("horoscope_trigger", "content")
				    .withSchedule(CronScheduleBuilder
				    		.cronSchedule(AppConfig.getInstance().getSchedHoroscope()))
				    .build();
			log.info("Horoscope job scheduled");
			
			// Weather
			JobDetail weatherJob = JobBuilder.newJob(WeatherJob.class)
					.withIdentity("weather_job", "content")
					.build();
			CronTrigger weatherTrigger = TriggerBuilder.newTrigger()
					.withIdentity("weather_trigger", "content")
				    .withSchedule(CronScheduleBuilder
				    		.cronSchedule(AppConfig.getInstance().getSchedWeather()))
				    .build();
			log.info("Weather job scheduled");
			
			// Soapie
			JobDetail soapieJob = JobBuilder.newJob(SoapieJob.class)
					.withIdentity("soapie_job", "content")
					.build();
			CronTrigger soapieTrigger = TriggerBuilder.newTrigger()
					.withIdentity("soapie_trigger", "content")
					.withSchedule(CronScheduleBuilder
							.cronSchedule(AppConfig.getInstance().getSchedSoapie()))
					.build();
			log.info("Soapie job scheduled");
			
			// Zangoma
			JobDetail zangomaJob = JobBuilder.newJob(ZangomaJob.class)
					.withIdentity("zangoma_job", "content")
					.build();
			CronTrigger zangomaTrigger = TriggerBuilder.newTrigger()
					.withIdentity("zangoma_trigger", "content")
					.withSchedule(CronScheduleBuilder
							.cronSchedule(AppConfig.getInstance().getSchedZangoma()))
			.build();
			log.info("Zangoma job scheduled");

			// Schedule the jobs
			scheduler.scheduleJob(horoscopeJob, horoscopeTrigger);
			scheduler.scheduleJob(weatherJob, weatherTrigger);
			scheduler.scheduleJob(soapieJob, soapieTrigger);
			scheduler.scheduleJob(zangomaJob, zangomaTrigger);
		} catch (Exception e) {
			log.fatal("Unable to schedule automated jobs", e);
			System.exit(1);
		}
	}
	
	protected void stopServer() {
		try {
			server.stop();
			System.exit(0);
		} catch (Exception e) {
			System.exit(1);
		}
	}
	
	protected void shutdown() {
		log.info("Server shutting down. Executing shutdown sequence.");
		
		try {
			scheduler.shutdown(true);
		} catch (Exception e) {
			log.error("Scheduler failed to shutdown", e);
		}
		
		try {
			cleanupTimer.cancel();
		} catch (Exception e) {}
	}
	
	protected class CleanupThread implements Runnable {
		@Override
		public void run() {
			System.gc();
		}
	}

	@Override
	public void init(DaemonContext ctx) throws DaemonInitException, Exception {
		log.info("Server starting up");
		initServer();
	}

	@Override
	public void start() throws Exception {
		log.info("Server successfully started and listenting for requests on port: " + PORT);
	}

	@Override
	public void stop() throws Exception {
		shutdown();
		
	}
	
	@Override
	public void destroy() {
		log.info("Server has been shut down sucessfully and is now offline.");
	}

}