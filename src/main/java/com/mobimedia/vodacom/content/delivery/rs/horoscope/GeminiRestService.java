package com.mobimedia.vodacom.content.delivery.rs.horoscope;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mobimedia.vodacom.content.delivery.managers.TaskExecutionManager;
import com.mobimedia.vodacom.content.delivery.utils.AuthorizationUtil;

@Path("/horo/gem")
public class GeminiRestService {
	private static final Logger log = Logger.getLogger(GeminiRestService.class);
	
	@Produces({ "application/json" })
	@GET
	public Response triggerSend(@Context HttpServletRequest request) {
		// Make sure only authorized access is allowed
		if (!AuthorizationUtil.isAuthorized(request)) {
			return Response.status(401).build();
		}
		
		TaskExecutionManager.getInstance().executeHoroscopeGeminiSend();
		
		log.info("Web Service initiating Horoscope send for Gemini");
		
		return Response.accepted().build();
	}
}