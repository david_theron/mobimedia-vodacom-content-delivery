package com.mobimedia.vodacom.content.delivery.composer;

import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_AQUARIUS;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_ARIES;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_CANCER;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_CAPRICORN;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_GEMINI;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_LEO;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_LIBRA;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_PISCES;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_SAGITTARIUS;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_SCORPIO;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_TAURUS;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_HOROSCOPE_VIRGO;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_HOROSCOPE_AQUARIUS;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_HOROSCOPE_ARIES;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_HOROSCOPE_CANCER;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_HOROSCOPE_CAPRICORN;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_HOROSCOPE_GEMINI;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_HOROSCOPE_LEO;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_HOROSCOPE_LIBRA;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_HOROSCOPE_PISCES;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_HOROSCOPE_SAGITTARIUS;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_HOROSCOPE_SCORPIO;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_HOROSCOPE_TAURUS;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.mobimedia.vodacom.content.delivery.config.AppConfig;
import com.mobimedia.vodacom.content.delivery.contenttype.Horoscope;
import com.mobimedia.vodacom.content.delivery.entities.SubmitTaskItem;
import com.mobimedia.vodacom.content.delivery.utils.HibernateUtil;

public class HoroscopeContentComposer {
	private static final Logger log = Logger.getLogger(HoroscopeContentComposer.class);
	
	private Horoscope horoscope;
	private Date date;
	
	public HoroscopeContentComposer(Horoscope horoscope, Date date) {
		this.horoscope = horoscope;
		this.date = date;
	}
	
	@SuppressWarnings("unchecked")
	public SubmitTaskItem getSubmitTaskItem() {
		List<String> recipients = new ArrayList<String>();
		String serviceId = null;
		String message = null;
		String fusionSubName = null;
		
		Session session = null;
		SubmitTaskItem item = null;
		
		try {
        	session = HibernateUtil.getSessionFactory().openSession();
		
			switch (horoscope) {
				case AQUARIUS:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
			        		.setParameter("channel", "Aquarius")
			        		.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_AQUARIUS)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Aquarius Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Aquarius")
							.list();
			        
			        log.info("Horoscope Aquarius recipients = " + recipients.size());
					
			        serviceId = SERVICEID_HOROSCOPE_AQUARIUS;
			        fusionSubName = FUSION_SUBNAME_HOROSCOPE_AQUARIUS;
				}
				break;
					
				case ARIES:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
							.setParameter("channel", "Aries")
							.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_ARIES)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Aries Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
			        		.setParameter("stype", "Aries")
			        		.list();
			        
			        log.info("Horoscope Aries recipients = " + recipients.size());
					
			        serviceId = SERVICEID_HOROSCOPE_ARIES;
			        fusionSubName = FUSION_SUBNAME_HOROSCOPE_ARIES;
				}
				break;
					
				case CANCER:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
							.setParameter("channel", "Cancer")
							.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_CANCER)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Cancer Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
			        
					recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Cancer")
							.list();
					
					log.info("Horoscope Cancer recipients = " + recipients.size());
					
					serviceId = SERVICEID_HOROSCOPE_CANCER;
					fusionSubName = FUSION_SUBNAME_HOROSCOPE_CANCER;
				}
				break;
				
				case CAPRICORN:
				{
			        List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
			        	.setParameter("channel", "Capricorn")
			        	.list();
			        
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_CAPRICORN)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Capricorn Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
			        
					recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Capricorn")
							.list();
					
					log.info("Horoscope Capricorn recipients = " + recipients.size());
					
					serviceId = SERVICEID_HOROSCOPE_CAPRICORN;
					fusionSubName = FUSION_SUBNAME_HOROSCOPE_CAPRICORN;
				}	
				break;
				
				case GEMINI:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
				        	.setParameter("channel", "Gemini")
				        	.list();
				        
					message = text.get(0);
					
					List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_GEMINI)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Gemini Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
				        
					recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Gemini")
							.list();
					
					log.info("Horoscope Gemini recipients = " + recipients.size());
						
					serviceId = SERVICEID_HOROSCOPE_GEMINI;
					fusionSubName = FUSION_SUBNAME_HOROSCOPE_GEMINI;
				}
				break;
				
				case LEO:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
							.setParameter("channel", "Leo")
							.list();
		        
					message = text.get(0);
					
					List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_LEO)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Leo Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
		        
					recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Leo")
							.list();
					
					log.info("Horoscope Leo recipients = " + recipients.size());
				
					serviceId = SERVICEID_HOROSCOPE_LEO;
					fusionSubName = FUSION_SUBNAME_HOROSCOPE_LEO;
				}
				break;
				
				case LIBRA:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
							.setParameter("channel", "Libra")
							.list();
		        
					message = text.get(0);
					
					List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_LIBRA)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Libra Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
		        
					recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Libra")
							.list();
					
					log.info("Horoscope Libra recipients = " + recipients.size());
				
					serviceId = SERVICEID_HOROSCOPE_LIBRA;
					fusionSubName = FUSION_SUBNAME_HOROSCOPE_LIBRA;
				}
				break;
					
				case PISCES:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
							.setParameter("channel", "Pisces")
							.list();
		        
					message = text.get(0);
		        
					recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Pisces")
							.list();
					
					List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_PISCES)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Pisces Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
			        
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Pisces")
							.list();
					
					log.info("Horoscope Pisces recipients = " + recipients.size());
				
					serviceId = SERVICEID_HOROSCOPE_PISCES;
					fusionSubName = FUSION_SUBNAME_HOROSCOPE_PISCES;
				}
				break;
				
				case SAGITTARIUS:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
							.setParameter("channel", "Sagittarius")
							.list();
		        
					message = text.get(0);
					
					List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_SAGITTARIUS)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Sagittarius Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
		        
					recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Sagittarius")
							.list();
					
					log.info("Horoscope Sagittarius recipients = " + recipients.size());
				
					serviceId = SERVICEID_HOROSCOPE_SAGITTARIUS;
					fusionSubName = FUSION_SUBNAME_HOROSCOPE_SAGITTARIUS;
				}
				break;
					
				case SCORPIO:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
							.setParameter("channel", "Scorpio")
							.list();
		        
					message = text.get(0);
					
					List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_SCORPIO)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Scorpio Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
		        
					recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Scorpio")
							.list();
					
					log.info("Horoscope Scorpio recipients = " + recipients.size());
				
					serviceId = SERVICEID_HOROSCOPE_SCORPIO;
					fusionSubName = FUSION_SUBNAME_HOROSCOPE_SCORPIO;
				}
				break;
					
				case TAURUS:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
							.setParameter("channel", "Taurus")
							.list();
		        
					message = text.get(0);
					
					List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_TAURUS)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Taurus Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
		        
					recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Taurus")
							.list();
					
					log.info("Horoscope Taurus recipients = " + recipients.size());
				
					serviceId = SERVICEID_HOROSCOPE_TAURUS;
					fusionSubName = FUSION_SUBNAME_HOROSCOPE_TAURUS;
				}
				break;
				
				case VIRGO:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_horoscopes AS s WHERE channel = :channel")
							.setParameter("channel", "Virgo")
							.list();
		        
					message = text.get(0);
					
					List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_HOROSCOPE_VIRGO)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Virgo Horoscope message send detected, skipping send.");
				        	return null;
				        }
			        }
		        
					recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_horo AS s WHERE stype = :stype")
							.setParameter("stype", "Virgo")
							.list();
					
					log.info("Horoscope Virgo recipients = " + recipients.size());
				
					serviceId = SERVICEID_HOROSCOPE_TAURUS;
					fusionSubName = FUSION_SUBNAME_HOROSCOPE_VIRGO;
				}
				break;
					
				default:
					throw new RuntimeException("Unable to determine content type to send");
			}
			
			item = new SubmitTaskItem(recipients, serviceId, message, fusionSubName);
		} catch (Exception e) {
			// TODO determine which exception and handle appropriately (error queue for retry?)
			log.error(e);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {}
			}
			session = null;
		}
		
		return item;
	}

}
