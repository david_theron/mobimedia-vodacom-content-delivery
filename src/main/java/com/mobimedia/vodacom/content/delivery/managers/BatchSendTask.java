package com.mobimedia.vodacom.content.delivery.managers;

import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.List;

import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;

import za.co.vodacom.sgp.NVPair;

import com.mobimedia.vodacom.content.delivery.FusionWebService;
import com.mobimedia.vodacom.content.delivery.entities.FusionLogItem;

public class BatchSendTask implements Runnable {
	private static final Logger log = Logger.getLogger(BatchSendTask.class);
	
	private List<String> recipients = null;
	
	private  String serviceId = null;
	
	private String message = null;
	
	private String fusionSubName = null;
	
	private int batchNum;
	
	public BatchSendTask(List<String> recipients, 
			String serviceId, String message, String fusionSubName, int batchNum) {
		this.recipients = recipients;
		this.serviceId = serviceId;
		this.message = message;
		this.fusionSubName = fusionSubName;
		this.batchNum = batchNum;
	}
	
	@Override
	public void run() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String transactionId = fusionSubName + "_" + timestamp + "_b_" + batchNum;
		String response = new String();
		
		try {
			long startTime = System.currentTimeMillis();
			FusionWebService webService = new FusionWebService();
			NVPair[] result = webService.sendSmsAlert(recipients, serviceId, message, transactionId);
			for (NVPair nvpair : result) {
				response += String.format("%s=%s", nvpair.getName(), nvpair.getValue())+";";
			}
			long endTime = System.currentTimeMillis();
			long responseTime = endTime - startTime;
			log.info("Response received: "+response+" with a response time from Vodacom Fusion for TransactionId="+transactionId+": "+responseTime+"ms");
			
			StringBuilder msisdns = new StringBuilder();
			for (int i = 0; i < recipients.size(); i++) {
				msisdns.append(recipients.get(i));
				if (i < (recipients.size()-1)) {
					msisdns.append(",");
				}
			}
			
			FusionLogItem item = new FusionLogItem(response, msisdns.toString(), fusionSubName, 
					transactionId, recipients.size(), message, timestamp, responseTime, false);
			FusionLogManager.getInstance().queueLogItem(item);
		} catch (AxisFault e) {
			log.error("AxisFault while executing webservice call.", e);
			SendManager.getInstance().addToErrorQueue(this, e);
		} catch (RemoteException e) {
			log.error("RemoteException while executing webservice call.", e);
			SendManager.getInstance().addToErrorQueue(this, e);
		} catch (Exception e) {
			log.error(e.getClass().getSimpleName() +" while executing webservice call.", e);
			SendManager.getInstance().addToErrorQueue(this, e);
		}
	}
	
	public List<String> getRecipients() {
		return recipients;
	}
	
	public String getServiceId() {
		return serviceId;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String getFusionSubName() {
		return fusionSubName;
	}
	
	public int getBatchNum() {
		return batchNum;
	}
}
