package com.mobimedia.vodacom.content.delivery.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ussd_sport_daypass")
public class UssdSportDayPass {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String channel;
	
	private String ussdBody;
	
	private String time;
	
	public UssdSportDayPass() {
		
	}
	
	public UssdSportDayPass(Long id, String channel, String ussdBody, String time) {
		this.id = id;
		this.channel = channel;
		this.ussdBody = ussdBody;
		this.time = time;
	}
	
	@Column(name="id")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="channel")
	public String getChannel() {
		return channel;
	}
	
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	@Column(name="USSDbody")
	public String getUssdBody() {
		return ussdBody;
	}
	
	public void setUssdBody(String ussdBody) {
		this.ussdBody = ussdBody;
	}
	
	@Column(name="ts")
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
}