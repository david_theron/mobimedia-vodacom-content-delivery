package com.mobimedia.vodacom.content.delivery.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sms_zangoma")
public class SmsZangomaEnglish {
	
	@Id
	@GeneratedValue
	private Long num;
	
	private String text;
	
	public SmsZangomaEnglish() {
		
	}
	
	public SmsZangomaEnglish(Long num, String text) {
		this.num = num;
		this.text = text;
	}
	
	@Column(name="num")
	public Long getNum() {
		return num;
	}
	
	public void setNum(Long num) {
		this.num = num;
	}
	
	@Column(name="text")
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
}