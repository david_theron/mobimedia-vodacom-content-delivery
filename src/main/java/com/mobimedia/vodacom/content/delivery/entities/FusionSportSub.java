package com.mobimedia.vodacom.content.delivery.entities;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Fusion_Sport_sub")
public class FusionSportSub {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="response")
	private String response;
	
	@Column(name="msisdn")
	private String recipients;
	
	@Column(name="tid")
	private String tid;
	
	@Column(name="subtype")
	private String subtype;
	
	@Column(name="rtype")
	private String rtype;
	
	@Column(name="msg")
	private String msg;
	
	@Column(name="time")
	private Timestamp time;
	
	@Column(name="resendtime")
	private String resendTime;
	
	@Column(name="resendnum")
	private String resendNum;
	
	@Column(name="responsetime")
	private Long responseTime;
	
	public FusionSportSub() {
		
	}
	
	public FusionSportSub(Long id, String response, String recipients, String tid, 
			String subtype, String rtype, String msg, Timestamp time, 
			String resendTime, String resendNum, Long responseTime) {
		this.id = id;
		this.response = response;
		this.recipients = recipients;
		this.tid = tid;
		this.subtype = subtype;
		this.rtype = rtype;
		this.msg = msg;
		this.time = time;
		this.resendTime = resendTime;
		this.resendNum = resendNum;
		this.responseTime = responseTime;
	}
	
	public FusionSportSub(String response, String recipients, String subtype, String tid, String rtype, String msg, Timestamp time, Long responseTime) {
		this.response = response;
		this.recipients = recipients;
		this.subtype = subtype;
		this.tid = tid;
		this.rtype = rtype;
		this.msg = msg;
		this.time = time;
		this.responseTime = responseTime;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getResponse() {
		return response;
	}
	
	public void setResponse(String response) {
		this.response = response;
	}
	
	public String getRecipients() {
		return recipients;
	}
	
	public List<String> getRecipientsAsList() {
		if (recipients == null || recipients.trim().length() <= 0) {
			return null;
		}
		String[] recipSplit = recipients.split(",");
		return new ArrayList<String>(Arrays.asList(recipSplit));
	}
	
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}
	
	public String getTid() {
		return tid;
	}
	
	public void setTid(String tid) {
		this.tid = tid;
	}
	
	public String getSubtype() {
		return subtype;
	}
	
	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}
	
	public String getRtype() {
		return rtype;
	}
	
	public void setRtype(String rtype) {
		this.rtype = rtype;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public Timestamp getTime() {
		return time;
	}
	
	public void setTime(Timestamp time) {
		this.time = time;
	}
	
	public String getResendTime() {
		return resendTime;
	}
	
	public void setResendTime(String resendTime) {
		this.resendTime = resendTime;
	}
	
	public String getResendNum() {
		return resendNum;
	}
	
	public void setResendNum(String resendNum) {
		this.resendNum = resendNum;
	}
	
	public Long getResponseTime() {
		return responseTime;
	}
	
	public void setResponseTime(Long responseTime) {
		this.responseTime = responseTime;
	}
}