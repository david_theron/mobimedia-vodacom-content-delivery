package com.mobimedia.vodacom.content.delivery.composer;

import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_SOAPIE_7DELAAN;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_SOAPIE_GENERATIONS;
import static com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig.FUSION_SUBNAME_SOAPIE_ISIDINGO;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_SOAPIE_7DELAAN;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_SOAPIE_GENERATIONS;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_SOAPIE_ISIDINGO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.mobimedia.vodacom.content.delivery.config.AppConfig;
import com.mobimedia.vodacom.content.delivery.contenttype.Soapie;
import com.mobimedia.vodacom.content.delivery.entities.SubmitTaskItem;
import com.mobimedia.vodacom.content.delivery.utils.HibernateUtil;

public class SoapieContentComposer {
	
	private static final Logger log = Logger.getLogger(SoapieContentComposer.class);
	
	private Soapie soapie;
	private Date date;
	
	public SoapieContentComposer(Soapie soapie, Date date) {
		this.soapie = soapie;
		this.date = date;
	}
	
	@SuppressWarnings("unchecked")
	public SubmitTaskItem getSubmitTaskItem() {
		List<String> recipients = new ArrayList<String>();
		String serviceId = null;
		String message = null;
		String fusionSubName = null;
		
		Session session = null;
		SubmitTaskItem item = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			
			switch (soapie) {
				case GENERATIONS:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_soapie_updates AS s WHERE channel = :channel")
			        		.setParameter("channel", "Generations")
			        		.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_SOAPIE_GENERATIONS)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Generations Soapie message send detected, skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_soap AS s WHERE stype = :stype")
							.setParameter("stype", "Generations")
							.list();
			        
			        log.info("Soapie Generations recipients = " + recipients.size());
					
			        serviceId = SERVICEID_SOAPIE_GENERATIONS;
			        fusionSubName = FUSION_SUBNAME_SOAPIE_GENERATIONS;
				}
				break;
				
				case ISIDINGO:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_soapie_updates AS s WHERE channel = :channel")
			        		.setParameter("channel", "Isidingo")
			        		.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_SOAPIE_ISIDINGO)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate Isidingo Soapie message send detected, skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_soap AS s WHERE stype = :stype")
							.setParameter("stype", "Isidingo")
							.list();
			        
			        log.info("Soapie Isidingo recipients = " + recipients.size());
					
			        serviceId = SERVICEID_SOAPIE_ISIDINGO;
			        fusionSubName = FUSION_SUBNAME_SOAPIE_ISIDINGO;
				}
				break;
				
				case SEWENDELAAN:
				{
					List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_soapie_updates AS s WHERE channel = :channel")
			        		.setParameter("channel", "7de Laan")
			        		.list();
					
			        message = text.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_sub AS s WHERE sname=:sname ORDER BY id DESC LIMIT 1")
			        		.setParameter("sname", FUSION_SUBNAME_SOAPIE_7DELAAN)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate 7de Laan Soapie message send detected, skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_soap AS s WHERE stype = :stype")
							.setParameter("stype", "7de Laan")
							.list();
			        
			        log.info("Soapie 7de Laan recipients = " + recipients.size());
					
			        serviceId = SERVICEID_SOAPIE_7DELAAN;
			        fusionSubName = FUSION_SUBNAME_SOAPIE_7DELAAN;
				}
				break;
					
				default:
					throw new RuntimeException("Unable to determine content type to send");
			}
			
			item = new SubmitTaskItem(recipients, serviceId, message, fusionSubName);
		} catch (Exception e) {
			// TODO determine which exception and handle appropriately (error queue for retry?)
			log.error(e);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {}
			}
			session = null;
		}
		
		return item;
	}
}
