package com.mobimedia.vodacom.content.delivery.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ussd_users")
public class UssdUser {
	@Id
	@GeneratedValue
	private Long id;
	
	private String msisdn;
	
	private String sid;
	
	private String level;
	
	private String cselect;
	
	private String rtype;
	
	private String msg;
	
	private String time;
	
	private String once;
	
	private String onceHoroscopes;
	
	private String onceZangoma;
	
	private String onceNews;
	
	private String onceSoapies;
	
	private String onceWeather;
	
	public UssdUser() {
		
	}
	
	public UssdUser(Long id, String msisdn, String sid, String level, String cselect, 
			String rtype, String msg, String time, String once, String onceHoroscopes, 
			String onceZangoma, String onceNews, String onceSoapies, String onceWeather) {
		this.id = id;
		this.msisdn = msisdn;
		this.sid = sid;
		this.level = level;
		this.cselect = cselect;
		this.rtype = rtype;
		this.msg = msg;
		this.time = time;
		this.once = once;
		this.onceHoroscopes = onceHoroscopes;
		this.onceZangoma = onceZangoma;
		this.onceNews = onceNews;
		this.onceSoapies = onceSoapies;
		this.onceWeather = onceWeather;
	}
	
	@Column(name="id")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="msisdn")
	public String getMsisdn() {
		return msisdn;
	}
	
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	
	@Column(name="sid")
	public String getSid() {
		return sid;
	}
	
	public void setSid(String sid) {
		this.sid = sid;
	}
	
	@Column(name="level")
	public String getLevel() {
		return level;
	}
	
	public void setLevel(String level) {
		this.level = level;
	}
	
	@Column(name="cselect")
	public String getCselect() {
		return cselect;
	}
	
	public void setCselect(String cselect) {
		this.cselect = cselect;
	}
	
	@Column(name="rtype")
	public String getRtype() {
		return rtype;
	}
	
	public void setRtype(String rtype) {
		this.rtype = rtype;
	}
	
	@Column(name="msg")
	public String getMsg() {
		return msg;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	@Column(name="time")
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
	
	@Column(name="once")
	public String getOnce() {
		return once;
	}
	
	public void setOnce(String once) {
		this.once = once;
	}
	
	@Column(name="once_horo")
	public String getOnceHoroscopes() {
		return onceHoroscopes;
	}
	
	public void setOnceHoroscopes(String onceHoroscopes) {
		this.onceHoroscopes = onceHoroscopes;
	}
	
	@Column(name="once_zang")
	public String getOnceZangoma() {
		return onceZangoma;
	}
	
	public void setOnceZangoma(String onceZangoma) {
		this.onceZangoma = onceZangoma;
	}
	
	@Column(name="once_news")
	public String getOnceNews() {
		return onceNews;
	}
	
	public void setOnceNews(String onceNews) {
		this.onceNews = onceNews;
	}
	
	@Column(name="once_soap")
	public String getOnceSoapies() {
		return onceSoapies;
	}
	
	public void setOnceSoapies(String onceSoapies) {
		this.onceSoapies = onceSoapies;
	}
	
	@Column(name="once_weat")
	public String getOnceWeather() {
		return onceWeather;
	}
	
	public void setOnceWeather(String onceWeather) {
		this.onceWeather = onceWeather;
	}
}