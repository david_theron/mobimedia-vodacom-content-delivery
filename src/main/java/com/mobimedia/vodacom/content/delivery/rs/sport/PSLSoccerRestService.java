package com.mobimedia.vodacom.content.delivery.rs.sport;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.mobimedia.vodacom.content.delivery.managers.TaskExecutionManager;
import com.mobimedia.vodacom.content.delivery.utils.AuthorizationUtil;
import com.mobimedia.vodacom.content.delivery.utils.HibernateUtil;

@Path("/sport/soccer/psl")
public class PSLSoccerRestService {
	private static final Logger log = Logger.getLogger(PSLSoccerRestService.class);
	
	@QueryParam("id") int id;
	
	@SuppressWarnings("unchecked")
	@Produces({ "application/json" })
	@GET
	public Response triggerSend(@Context HttpServletRequest request) {
		// Make sure only authorized access is allowed
		if (!AuthorizationUtil.isAuthorized(request)) {
			return Response.status(401).build();
		}
		
		// Check whether the content exists.
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			List<String> text = session.createSQLQuery("Select s.SMSbody FROM sms_football_psl_matchpass AS s WHERE id = :id")
					.setParameter("id", id)
					.list();
			if (text == null || text.size() <= 0 || text.get(0) == null || text.get(0).trim().isEmpty()) {
				return Response.status(400).entity("No content for id="+id).build();
			}
		} catch (Exception e) {
			log.error("Error while trying to check for PSL content", e);
			return Response.status(500).entity("Unexpected server error").build();
		} finally {
			if (session != null) {
				session.close();
			}
			session = null;
		}
				
		TaskExecutionManager.getInstance().executePSLSoccerSend(id);
		
		log.info("Web Service initiating PSL Soccer send for id="+id);
		
		return Response.accepted().build();
	}
}