package com.mobimedia.vodacom.content.delivery.entities;

import java.util.List;

public class SubmitTaskItem {
	
	private List<String> recipients;
	
	private String serviceId;
	
	private String message;
	
	private String fusionSubName;

	public SubmitTaskItem(List<String> recipients, String serviceId, String message, String fusionSubName) {
		this.recipients = recipients;
		this.serviceId = serviceId;
		this.message = message;
		this.fusionSubName = fusionSubName;
	}
	
	public List<String> getRecipients() {
		return recipients;
	}
	
	public String getServiceId() {
		return serviceId;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String getFusionSubName() {
		return fusionSubName;
	}
}
