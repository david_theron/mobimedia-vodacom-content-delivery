package com.mobimedia.vodacom.content.delivery.config;

public final class FusionSubNameConfig {
	/*
	 * Horoscopes
	 */
	public static final String FUSION_SUBNAME_HOROSCOPE_AQUARIUS 		= "Horo_aqu";
	public static final String FUSION_SUBNAME_HOROSCOPE_ARIES	 		= "Horo_ari";
	public static final String FUSION_SUBNAME_HOROSCOPE_CANCER	 		= "Horo_can";
	public static final String FUSION_SUBNAME_HOROSCOPE_CAPRICORN		= "Horo_cap";
	public static final String FUSION_SUBNAME_HOROSCOPE_GEMINI			= "Horo_gem";
	public static final String FUSION_SUBNAME_HOROSCOPE_LEO				= "Horo_leo";
	public static final String FUSION_SUBNAME_HOROSCOPE_LIBRA			= "Horo_lib";
	public static final String FUSION_SUBNAME_HOROSCOPE_PISCES			= "Horo_pis";
	public static final String FUSION_SUBNAME_HOROSCOPE_SAGITTARIUS		= "Horo_sag";
	public static final String FUSION_SUBNAME_HOROSCOPE_SCORPIO			= "Horo_sco";
	public static final String FUSION_SUBNAME_HOROSCOPE_TAURUS			= "Horo_tau";
	public static final String FUSION_SUBNAME_HOROSCOPE_VIRGO			= "Horo_vir";
	
	/*
	 * Soapies 
	 */
	public static final String FUSION_SUBNAME_SOAPIE_7DELAAN			= "Soap_7de";
	public static final String FUSION_SUBNAME_SOAPIE_GENERATIONS		= "Soap_gen";
	public static final String FUSION_SUBNAME_SOAPIE_ISIDINGO			= "Soap_isi";
	
	/*
	 * Weather
	 */
	public static final String FUSION_SUBNAME_WEATHER_BLOEMFONTEIN 		= "Weat_bfn";
	public static final String FUSION_SUBNAME_WEATHER_CAPE_TOWN 		= "Weat_cpt";
	public static final String FUSION_SUBNAME_WEATHER_DURBAN			= "Weat_dbn";
	public static final String FUSION_SUBNAME_WEATHER_JOHANNESBURG 		= "Weat_jhb";
	public static final String FUSION_SUBNAME_WEATHER_PRETORIA			= "Weat_pta";
	public static final String FUSION_SUBNAME_WEATHER_PORT_ELIZABETH 	= "Weat_pte";
	
	/*
	 * Zangoma
	 */
	public static final String FUSION_SUBNAME_ZANGOMA_ENGLISH			= "Zang_English";
	public static final String FUSION_SUBNAME_ZANGOMA_ZULU				= "Zang_Zulu";
	
	/**
	 * Determine whether this is sports content.
	 * 
	 * @param fusionSubName
	 * @return
	 */
	public static boolean isSport(String fusionSubName) {
		if (fusionSubName.startsWith("epl") || fusionSubName.startsWith("psl")) {
			return true;
		} else {
			return false;
		}
	}
}
