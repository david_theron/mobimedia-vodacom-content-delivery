package com.mobimedia.vodacom.content.delivery.rs.error;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.mobimedia.vodacom.content.delivery.managers.BatchSendTask;
import com.mobimedia.vodacom.content.delivery.managers.SendManager;
import com.mobimedia.vodacom.content.delivery.managers.SendManager.BatchSendErrorItem;
import com.mobimedia.vodacom.content.delivery.utils.AuthorizationUtil;

@Path("/error/send")
public class SendManagerErrorRestService {
	private static final Logger log = Logger.getLogger(SendManagerErrorRestService.class);
	
	@Produces({ "application/json" })
	@GET
	public Response send(@Context HttpServletRequest request) {
		// Make sure only authorized access is allowed
		if (!AuthorizationUtil.isAuthorized(request)) {
			return Response.status(401).build();
		}
		
		List<BatchSendErrorItem> errors = SendManager.getInstance().getErrorsAsList();
		
		Gson gson = new Gson();
		List<JsonResponseItem> responseItems = new ArrayList<JsonResponseItem>();
		for (BatchSendErrorItem item : errors) {
			responseItems.add(new JsonResponseItem(item));
		}
		String response = gson.toJson(responseItems);
		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}
	
	public class JsonResponseItem {
		
		private String exceptionType;
		private String exceptionMessage;
		private List<String> recipients;
		private String serviceId;
		private String message;
		private String fusionSubName;
		private int batchNum;
		
		public JsonResponseItem(BatchSendErrorItem item) {
			BatchSendTask task = item.getTask();
			if (task != null) {
				recipients = task.getRecipients();
				serviceId = task.getServiceId();
				message = task.getMessage();
				fusionSubName = task.getFusionSubName();
				batchNum = task.getBatchNum();
			}
			
			Exception exception = item.getException();
			if (exception != null) {
				exceptionType = exception.getClass().toString();
				exceptionMessage = exception.getMessage();
			}
		}
	}
}
