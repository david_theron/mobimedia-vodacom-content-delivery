package com.mobimedia.vodacom.content.delivery.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.mobimedia.vodacom.content.delivery.entities.FusionLogItem;
import com.mobimedia.vodacom.content.delivery.managers.SendManager.BatchSendErrorItem;

public class FusionLogManager {
	private static final Logger log = Logger.getLogger(SendManager.class);
	
	private static FusionLogManager instance = null;
	
	private static final int CORE_POOL_SIZE = 15;
	private static final int MAX_POOL_SIZE = 40;
	private static final int KEEP_ALIVE_TIME = 5000;
	
	private ExecutorService executorService = null;
	
	private final Queue<FusionLogErrorItem> errorQueue = 
			new ConcurrentLinkedQueue<FusionLogErrorItem>();
	
	private FusionLogManager() {
		executorService = new ThreadPoolExecutor(
				CORE_POOL_SIZE, MAX_POOL_SIZE, 
				KEEP_ALIVE_TIME, TimeUnit.MILLISECONDS, 
				new LinkedBlockingQueue<Runnable>());
	}
	
	public List<FusionLogErrorItem> getErrorsAsList() {
		List<FusionLogErrorItem> errors = new ArrayList<FusionLogErrorItem>();
		Object[] errorsArray = errorQueue.toArray();
		for (int i=0; i < errorsArray.length; i++) {
			errors.add((FusionLogErrorItem)errorsArray[i]);
		}
		return errors;
	}
	
	public static FusionLogManager getInstance() {
		if (instance == null) {
			instance = new FusionLogManager();
		}
		return instance;
	}
	
	public void queueLogItem(FusionLogItem item) {
		FusionLogTask task = new FusionLogTask(item);
		try {
			executorService.execute(new FusionLogTask(item));
		} catch (Exception e) {
			log.error("Exception while trying to queue log item", e);
			addToErrorQueue(task, e);
		}
	}
	
	public void addToErrorQueue(FusionLogTask fusionLogTask, Exception e) {
		errorQueue.add(new FusionLogErrorItem(fusionLogTask, e));
	}
	
	public void reinjectErrors() {
		while (!errorQueue.isEmpty()) {
			FusionLogErrorItem item = errorQueue.poll();
			if (item != null) {
				FusionLogTask task = item.getTask();
				if (task != null) {
					try {
						executorService.execute(task);
					} catch (Exception e) {
						log.error(e);
						addToErrorQueue(task, e);
					}
				}
			}
			try {
				Thread.sleep(100);
			} catch (Exception e) {}
		}
	}
	
	public class FusionLogErrorItem {
		private FusionLogTask task;
		private Exception exception;
		
		public FusionLogErrorItem(FusionLogTask task, Exception exception) {
			this.task = task;
			this.exception = exception;
		}
		
		public FusionLogTask getTask() {
			return task;
		}
		
		public Exception getException() {
			return exception;
		}
	}
}