package com.mobimedia.vodacom.content.delivery.contenttype;

public enum Horoscope {
	AQUARIUS,
	ARIES,
	CANCER,
	CAPRICORN,
	GEMINI,
	LEO,
	LIBRA,
	PISCES,
	SAGITTARIUS,
	SCORPIO,
	TAURUS,
	VIRGO
}
