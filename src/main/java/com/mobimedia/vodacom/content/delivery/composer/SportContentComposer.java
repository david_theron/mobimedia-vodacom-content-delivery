package com.mobimedia.vodacom.content.delivery.composer;

import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_SPORT_EPL;
import static com.mobimedia.vodacom.content.delivery.config.ServiceIdConfig.SERVICEID_SPORT_PSL;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.mobimedia.vodacom.content.delivery.config.AppConfig;
import com.mobimedia.vodacom.content.delivery.contenttype.Sport;
import com.mobimedia.vodacom.content.delivery.entities.SubmitTaskItem;
import com.mobimedia.vodacom.content.delivery.utils.HibernateUtil;

public class SportContentComposer {
	private static final Logger log = Logger.getLogger(SportContentComposer.class);
	
	private Sport sport;
	private Long id;
	
	public SportContentComposer(Sport sport, long id) {
		this.sport = sport;
		this.id = id;
	}
	
	@SuppressWarnings("unchecked")
	public SubmitTaskItem getSubmitTaskItem() {
		List<String> recipients = new ArrayList<String>();
		String serviceId = null;
		String message = null;
		String subType = null;
		String fusionSubName = null;
		
		Session session = null;
		SubmitTaskItem item = null;
		
		try {
        	session = HibernateUtil.getSessionFactory().openSession();
		
			switch (sport) {
				case EPL:
				{
					List<String> textRes = session.createSQLQuery("Select s.SMSbody FROM sms_football_epl_matchpass AS s WHERE id = :id")
			        		.setParameter("id", id)
			        		.list();
					
					List<String> subTypeRes = session.createSQLQuery("Select s.channel FROM sms_football_epl_matchpass AS s WHERE id = :id")
			        		.setParameter("id", id)
			        		.list();
					
			        message = textRes.get(0);
			        subType = "epl"+subTypeRes.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_Sport_sub AS s WHERE subtype=:subtype ORDER BY id DESC LIMIT 1")
			        		.setParameter("subtype", subType)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate EPL message(id="+id+") send detected for subtype="+subType+", skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_spor AS s WHERE stype = :stype")
							.setParameter("stype", subType)
							.list();
			        
			        log.info("Sport "+subType+" recipients = " + recipients.size());
					
			        serviceId = String.format(SERVICEID_SPORT_EPL, id);
			        fusionSubName = subType;
				}
				break;
				
				case PSL:
				{
					List<String> textRes = session.createSQLQuery("Select s.SMSbody FROM sms_football_psl_matchpass AS s WHERE id = :id")
			        		.setParameter("id", id)
			        		.list();
					
					List<String> subTypeRes = session.createSQLQuery("Select s.channel FROM sms_football_psl_matchpass AS s WHERE id = :id")
			        		.setParameter("id", id)
			        		.list();
					
			        message = textRes.get(0);
			        subType = "psl"+subTypeRes.get(0);
			        
			        List<String> textLog = session.createSQLQuery("Select s.msg FROM Fusion_Sport_sub AS s WHERE subtype=:subtype ORDER BY id DESC LIMIT 1")
			        		.setParameter("subtype", subType)
			        		.list();
			        if (!AppConfig.getInstance().skipDupicateContentCheck() && textLog != null && textLog.size() > 0) {
				        String logMessage = textLog.get(0);
				        if (logMessage != null && message.equals(logMessage)) {
				        	log.info("Duplicate PSL message(id="+id+") send detected for subtype="+subType+", skipping send.");
				        	return null;
				        }
			        }
					
			        recipients = session.createSQLQuery("SELECT s.msisdn FROM Subscriber_spor AS s WHERE stype = :stype")
							.setParameter("stype", subType)
							.list();
			        
			        log.info("Sport "+subType+" recipients = " + recipients.size());
					
			        serviceId = String.format(SERVICEID_SPORT_PSL, id);
			        fusionSubName = subType;
				}
				break;
					
				default:
					throw new RuntimeException("Unable to determine content type to send");
			}
			
			item = new SubmitTaskItem(recipients, serviceId, message, fusionSubName);
		} catch (Exception e) {
			// TODO determine which exception and handle appropriately (error queue for retry?)
			log.error(e);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {}
			}
			session = null;
		}
		return item;
	}

}
