package com.mobimedia.vodacom.content.delivery.config;

import java.util.Arrays;

import javax.ws.rs.ext.RuntimeDelegate;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mobimedia.vodacom.content.delivery.WebServiceApplication;
import com.mobimedia.vodacom.content.delivery.rs.error.FusionLogErrorReinjectRestService;
import com.mobimedia.vodacom.content.delivery.rs.error.FusionLogErrorRestService;
import com.mobimedia.vodacom.content.delivery.rs.error.SendManagerErrorReinjectRestService;
import com.mobimedia.vodacom.content.delivery.rs.error.SendManagerErrorRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.AllHoroscopeRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.AquariusRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.AriesRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.CancerRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.CapricornRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.GeminiRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.LeoRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.LibraRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.PiscesRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.SagittariusRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.ScorpioRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.TaurusRestService;
import com.mobimedia.vodacom.content.delivery.rs.horoscope.VirgoRestService;
import com.mobimedia.vodacom.content.delivery.rs.soapies.AllSoapieRestService;
import com.mobimedia.vodacom.content.delivery.rs.soapies.GenerationsRestService;
import com.mobimedia.vodacom.content.delivery.rs.soapies.IsidingoRestService;
import com.mobimedia.vodacom.content.delivery.rs.soapies.SewendeLaanRestService;
import com.mobimedia.vodacom.content.delivery.rs.sport.EPLSoccerRestService;
import com.mobimedia.vodacom.content.delivery.rs.sport.PSLSoccerRestService;
import com.mobimedia.vodacom.content.delivery.rs.weather.AllWeatherRestService;
import com.mobimedia.vodacom.content.delivery.rs.weather.BloemfonteinRestService;
import com.mobimedia.vodacom.content.delivery.rs.weather.CapeTownRestService;
import com.mobimedia.vodacom.content.delivery.rs.weather.DurbanRestService;
import com.mobimedia.vodacom.content.delivery.rs.weather.JohannesburgRestService;
import com.mobimedia.vodacom.content.delivery.rs.weather.PortElizabethRestService;
import com.mobimedia.vodacom.content.delivery.rs.weather.PretoriaRestService;
import com.mobimedia.vodacom.content.delivery.rs.zangoma.AllZangomaRestService;
import com.mobimedia.vodacom.content.delivery.rs.zangoma.ZangomaEnglishRestService;
import com.mobimedia.vodacom.content.delivery.rs.zangoma.ZangomaZuluRestService;

@Configuration
public class WebServiceConfig {
	@Bean(destroyMethod="shutdown")
	public SpringBus cxf() {
		return new SpringBus();
	}
	
	@Bean
	public Server jaxRsServer() {
		JAXRSServerFactoryBean factory = RuntimeDelegate.getInstance().createEndpoint(jaxWebServiceApplication(), JAXRSServerFactoryBean.class);
		factory.setServiceBeans(Arrays.<Object>asList(
				// Sport
				eplSoccerRestService(),
				pslSoccerRestService(),
				
				// Horoscopes
				allHoroscopeRestService(),
				aquariusRestService(), 
				ariesRestService(), 
				cancerRestService(), 
				capricornRestService(),
				geminiRestService(),
				leoRestService(),
				libraRestService(),
				piscesRestService(),
				sagittariusRestService(),
				scorpioRestService(),
				taurusRestService(),
				virgoRestService(),
				
				// Soapies
				allSoapieRestService(),
				generationsRestService(),
				isidingoRestService(),
				sewendeLaanRestService(),
				
				// Weather
				allWeatherRestService(),
				bloemfonteinRestService(),
				capeTownRestService(),
				durbanRestService(),
				johannesburgRestService(),
				portElizabethRestService(),
				pretoriaRestService(),
				
				// Zangoma
				allZangomaRestService(),
				zangomaEnglishRestService(),
				zangomaZuluRestService(),
				
				// System
				sendManagerErrorRestService(),
				sendManagerErrorReinjectRestService(),
				fusionLogErrorRestService(),
				fusionLogErrorReinjectRestService()
			)
		);
		factory.setAddress("/" + factory.getAddress());
		factory.setProviders(Arrays.<Object>asList(jsonProvider()));
		return factory.create();
	}
	
	@Bean
	public WebServiceApplication jaxWebServiceApplication() {
		return new WebServiceApplication();
	}
	
	@Bean
	public JacksonJsonProvider jsonProvider() {
		return new JacksonJsonProvider();
	}
	
	public EPLSoccerRestService eplSoccerRestService() {
		return new EPLSoccerRestService();
	}
	
	public PSLSoccerRestService pslSoccerRestService() {
		return new PSLSoccerRestService();
	}
	
	public AllHoroscopeRestService allHoroscopeRestService() {
		return new AllHoroscopeRestService();
	}
	
	public AquariusRestService aquariusRestService() {
		return new AquariusRestService();
	}
	
	public AriesRestService ariesRestService() {
		return new AriesRestService();
	}
	
	public CancerRestService cancerRestService() {
		return new CancerRestService();
	}
	
	public CapricornRestService capricornRestService() {
		return new CapricornRestService();
	}
	
	public GeminiRestService geminiRestService() {
		return new GeminiRestService();
	}
	
	public LeoRestService leoRestService() {
		return new LeoRestService();
	}
	
	public LibraRestService libraRestService() {
		return new LibraRestService();
	}
	
	public PiscesRestService piscesRestService() {
		return new PiscesRestService();
	}
	
	public SagittariusRestService sagittariusRestService() {
		return new SagittariusRestService();
	}
	
	public ScorpioRestService scorpioRestService() {
		return new ScorpioRestService();
	}
	
	public TaurusRestService taurusRestService() {
		return new TaurusRestService();
	}
	
	public VirgoRestService virgoRestService() {
		return new VirgoRestService();
	}
	
	public AllSoapieRestService allSoapieRestService() {
		return new AllSoapieRestService();
	}
	
	public GenerationsRestService generationsRestService() {
		return new GenerationsRestService();
	}
	
	public IsidingoRestService isidingoRestService() {
		return new IsidingoRestService();
	}
	
	public SewendeLaanRestService sewendeLaanRestService() {
		return new SewendeLaanRestService();
	}

	public AllWeatherRestService allWeatherRestService() {
		return new AllWeatherRestService();
	}
	
	public BloemfonteinRestService bloemfonteinRestService() {
		return new BloemfonteinRestService();
	}
	
	public CapeTownRestService capeTownRestService() {
		return new CapeTownRestService();
	}
	
	public DurbanRestService durbanRestService() {
		return new DurbanRestService();
	}
	
	public JohannesburgRestService johannesburgRestService() {
		return new JohannesburgRestService();
	}
	
	public PortElizabethRestService portElizabethRestService() {
		return new PortElizabethRestService();
	}
	
	public PretoriaRestService pretoriaRestService() {
		return new PretoriaRestService();
	}
	
	public AllZangomaRestService allZangomaRestService() {
		return new AllZangomaRestService();
	}
	
	public ZangomaEnglishRestService zangomaEnglishRestService() {
		return new ZangomaEnglishRestService();
	}
	
	public ZangomaZuluRestService zangomaZuluRestService() {
		return new ZangomaZuluRestService();
	}
	
	public SendManagerErrorRestService sendManagerErrorRestService() {
		return new SendManagerErrorRestService();
	}
	
	public SendManagerErrorReinjectRestService sendManagerErrorReinjectRestService() {
		return new SendManagerErrorReinjectRestService();
	}
	
	public FusionLogErrorRestService fusionLogErrorRestService() {
		return new FusionLogErrorRestService();
	}
	
	public FusionLogErrorReinjectRestService fusionLogErrorReinjectRestService() {
		return new FusionLogErrorReinjectRestService();
	}
}