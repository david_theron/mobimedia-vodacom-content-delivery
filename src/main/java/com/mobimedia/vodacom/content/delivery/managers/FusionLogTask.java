package com.mobimedia.vodacom.content.delivery.managers;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.mobimedia.vodacom.content.delivery.config.FusionSubNameConfig;
import com.mobimedia.vodacom.content.delivery.entities.FusionLogItem;
import com.mobimedia.vodacom.content.delivery.entities.FusionSportSub;
import com.mobimedia.vodacom.content.delivery.entities.FusionSub;
import com.mobimedia.vodacom.content.delivery.utils.HibernateUtil;

public class FusionLogTask implements Runnable {
	private static final Logger log = Logger.getLogger(BatchSendTask.class);
	
	private FusionLogItem item = null;
	
	public FusionLogTask(FusionLogItem item) {
		this.item = item;
	}
	
	public FusionLogItem getItem() {
		return item;
	}

	@Override
	public void run() {
		Session session = null;
		try {
        	session = HibernateUtil.getSessionFactory().openSession();
        	session.beginTransaction();
        	
        	Object fusionSub = null;
        	if (FusionSubNameConfig.isSport(item.getSubName())) {
        		fusionSub = new FusionSportSub(item.getResponse(), item.getMsisdns(), item.getSubName(), item.getTransactionId(), 
        				Integer.toString(item.getTotalSubscribers()), item.getMessage(), item.getTimestamp(), item.getResponseTime());
        	} else {
        		fusionSub = new FusionSub(item.getResponse(), item.getMsisdns(), item.getSubName(), item.getTransactionId(), 
        				Integer.toString(item.getTotalSubscribers()), item.getMessage(), item.getTimestamp(), item.getResponseTime());
        	}
        	
        	session.save(fusionSub);  
            session.getTransaction().commit();
		} catch (Exception e) {
			log.error("Exception while trying to insert fusion log", e);	
			FusionLogManager.getInstance().addToErrorQueue(this, e);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {}
			}
			session = null;
		}
	}

}
