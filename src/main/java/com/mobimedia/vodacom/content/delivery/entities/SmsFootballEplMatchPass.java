package com.mobimedia.vodacom.content.delivery.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sms_football_epl_matchpass")
public class SmsFootballEplMatchPass {

	@Id
	@GeneratedValue
	private Long id;
	
	private String channel;
	
	private String smsBody;
	
	private String time;
	
	public SmsFootballEplMatchPass() {
		
	}
	
	public SmsFootballEplMatchPass(Long id, String channel, String smsBody, String time) {
		this.id = id;
		this.channel = channel;
		this.smsBody = smsBody;
		this.time = time;
	}
	
	@Column(name="id")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="channel")
	public String getChannel() {
		return channel;
	}
	
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	@Column(name="SMSbody")
	public String getSmsBody() {
		return smsBody;
	}
	
	public void setSmsBody(String smsBody) {
		this.smsBody = smsBody;
	}
	
	@Column(name="ts")
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
}