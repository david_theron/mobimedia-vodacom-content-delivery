package com.mobimedia.vodacom.content.delivery.config;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.helpers.Loader;

public class AppConfig {
	private static final Logger log = Logger.getLogger(AppConfig.class);
	
	private static AppConfig instance = null;
	
	private boolean liveConfig = false;
	
	private String fusionEndpoint = null;
	private int fusionTimeout = 120000;
	private String fusionUsername = null;
	private String fusionPassword = null;
	
	private List<String> ipWhitelist = null;
	private String authUsername = null;
	private String authPassword = null;
	
	private String schedHoroscope = null;
	private String schedWeather = null;
	private String schedSoapie = null;
	private String schedZangoma = null;
	
	private boolean skipDuplicateContentCheck = false;
	private boolean batchSendEnabled = false;
	private int batchSendSize = 20;
	private int maxFusionConcurrentConnections = 40;
	
	Properties prop = null;
	
	private AppConfig() {
		load();
	}
	
	public static AppConfig getInstance() {
		if (instance == null) {
			instance = new AppConfig();
		}
		return instance;
	}
	
	private void load() {
		try {
			//load the properties file
			prop = new Properties();
			try {
				prop.load(new FileInputStream("/opt/vodacom-content-delivery/etc/config.properties"));
				log.info("Using configuration from file: /opt/vodacom-content-delivery/etc/config.properties");
			} catch (Exception e) {
				try {
					prop.load(AppConfig.class.getResourceAsStream("/config.properties"));
					log.info("Using configuration from file: jar:/config.properties");
				} catch (Exception eex) {
					prop.load(AppConfig.class.getResourceAsStream("config.properites"));
					log.info("Using configuration from file: jar:config.properties");
				}
			}
			
			// Whether it is live or test config.
			String config = prop.getProperty("active.config");
			liveConfig = (prop.getProperty("active.config").equals("live"));
			String environment = (liveConfig) ? "live" : "test";
			log.info("ENVIRONMENT IS CONFIGURED FOR: " + environment.toUpperCase());
			if (environment == null || !(environment.equals("live") || environment.equals("test"))) {
				throw new Exception("Invalid active config value");
			}
	
			// Fusion
			fusionEndpoint = prop.getProperty(environment+".fusion_endpoint");
			fusionTimeout = Integer.parseInt(prop.getProperty(environment+".fusion_timeout"));
			fusionUsername = prop.getProperty(environment+".fusion_username");
			fusionPassword = prop.getProperty(environment+".fusion_password");
			
			// App Auth
			String ipWhiteListString = prop.getProperty(environment+".auth_ip_whitelist");
			String ips[] = ipWhiteListString.split(",");
			ipWhitelist = Arrays.asList(ips);
			authUsername = prop.getProperty(environment+".auth_username");
			authPassword = prop.getProperty(environment+".auth_password");
			
			// Job Scheduling
			schedHoroscope = prop.getProperty(environment+".sched_horoscope");
			schedWeather = prop.getProperty(environment+".sched_weather");
			schedSoapie = prop.getProperty(environment+".sched_soapie");
			schedZangoma = prop.getProperty(environment+".sched_zangoma");
			
			skipDuplicateContentCheck = Boolean.parseBoolean(prop.getProperty(environment+".skip_duplicate_content_check"));
		} catch (Exception ex) {
			log.fatal("Unable to load application configuration: " + ex);
			System.exit(1);
	    }
	}
	
	public boolean isUsingLiveConfig() {
		return liveConfig;
	}
	
	public String getFusionEndpoint() {
		return fusionEndpoint;
	}
	
	public int getFusionTimeout() {
		return fusionTimeout;
	}
	
	public String getFusionUsername() {
		return fusionUsername;
	}
	
	public String getFusionPassword() {
		return fusionPassword;
	}
	
	public boolean isIPWhitelisted(String ip) {
		return ipWhitelist.contains(ip);
	}
	
	public boolean isAuthenticated(String username, String password) {
		return (authUsername.equals(username) && authPassword.equals(password));
	}
	
	public String getSchedHoroscope() {
		return schedHoroscope;
	}
	
	public String getSchedWeather() {
		return schedWeather;
	}
	
	public String getSchedSoapie() {
		return schedSoapie;
	}
	
	public String getSchedZangoma() {
		return schedZangoma;
	}
	
	public boolean skipDupicateContentCheck() {
		return skipDuplicateContentCheck;
	}
	
	public boolean isBatchSendEnabled() {
		return batchSendEnabled;
	}
	
	public int getBatchSendSize() {
		return batchSendSize;
	}
	
	public int getMaxFusionConcurrentConnections() {
		return maxFusionConcurrentConnections;
	}
}
