package com.mobimedia.vodacom.content.delivery.utils;

import javax.servlet.http.HttpServletRequest;

import org.apache.axis.encoding.Base64;
import org.apache.log4j.Logger;

import com.mobimedia.vodacom.content.delivery.config.AppConfig;

public final class AuthorizationUtil {
	
	private static final Logger log = Logger.getLogger(AuthorizationUtil.class);
	
	public static boolean isAuthorized(final HttpServletRequest request) {
		try {
			String ip = request.getRemoteAddr();
			if (!AppConfig.getInstance().isIPWhitelisted(ip)) {
				String auth = request.getHeader("Authorization");
				boolean authorized = false;
				if (auth != null) {
					if (auth.toLowerCase().startsWith("basic ")) {
						String token = auth.substring("basic ".length());
						String tokenDecoded = null;;
						try {
							tokenDecoded = new String(Base64.decode(token), "UTF-8");
						} catch (Exception e) {}
						if (tokenDecoded != null) {
							String[] credentials = tokenDecoded.split(":");
							if (credentials != null && credentials.length == 2) {
								String username = credentials[0];
								String password = credentials[1];
								if (username != null && password != null && 
										!username.trim().isEmpty() && !password.trim().isEmpty()) {
									if (AppConfig.getInstance().isAuthenticated(username, password)) {
										authorized = true;
									}
								}
							}
						}
					}
				}
				if (!authorized) {
					return false;
				} else {
					// Authorization is valid for username and password
					log.info("Auth Successful - Correct username and password combination");
				}
			} else {
				// IP Address is in whitelist
				log.info("Auth Successful - IP address is in whitelist: " + ip);
			}
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}

}
