package com.mobimedia.vodacom.content.delivery.managers;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.mobimedia.vodacom.content.delivery.contenttype.Horoscope;
import com.mobimedia.vodacom.content.delivery.contenttype.Soapie;
import com.mobimedia.vodacom.content.delivery.contenttype.Sport;
import com.mobimedia.vodacom.content.delivery.contenttype.Weather;
import com.mobimedia.vodacom.content.delivery.contenttype.Zangoma;

public class TaskExecutionManager {	
	private static TaskExecutionManager instance = null;
	
	private static final int CORE_POOL_SIZE = 10;
	private static final int MAX_POOL_SIZE = 10;
	private static final int KEEP_ALIVE_TIME = 5000;
	
	private ExecutorService executorService = null;
	
	private TaskExecutionManager() {
		executorService = new ThreadPoolExecutor(
				CORE_POOL_SIZE, MAX_POOL_SIZE, 
				KEEP_ALIVE_TIME, TimeUnit.MILLISECONDS, 
				new LinkedBlockingQueue<Runnable>());
	}
	
	public static TaskExecutionManager getInstance() {
		if (instance == null) {
			instance = new TaskExecutionManager();
		}
		return instance;
	}
	
	/**
	 * ERROR REINJECTION
	 */
	public void executeErrorSendReinject() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				SendManager.getInstance().reinjectErrors();
			}
		});
	}
	
	public void executeErrorFusionLogReinject() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				FusionLogManager.getInstance().reinjectErrors();
			}
		});
	}
	
	/**
	 * HOROSCOPES
	 */
	public void executeHoroscopeSend() {
		executeHoroscopeAquariusSend();
		executeHoroscopeAriesSend();
		executeHoroscopeCancerSend();
		executeHoroscopeCapricornSend();
		executeHoroscopeGeminiSend();
		executeHoroscopeLeoSend();
		executeHoroscopeLibraSend();
		executeHoroscopePiscesSend();
		executeHoroscopeSagittariusSend();
		executeHoroscopeScorpioSend();
		executeHoroscopeTaurusSend();
		executeHoroscopeVirgoSend();
	}
	
	public void executeHoroscopeAquariusSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.AQUARIUS, new Date());
			}
		});
	}
	
	public void executeHoroscopeAriesSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.ARIES, new Date());
			}
		});
	}
	
	public void executeHoroscopeCancerSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.CANCER, new Date());
			}
		});
	}
	
	public void executeHoroscopeCapricornSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.CAPRICORN, new Date());
			}
		});
	}
	
	public void executeHoroscopeGeminiSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.GEMINI, new Date());
			}
		});
	}
	
	public void executeHoroscopeLeoSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.LEO, new Date());
			}
		});
	}
	
	public void executeHoroscopeLibraSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.LIBRA, new Date());
			}
		});
	}
	
	public void executeHoroscopePiscesSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.PISCES, new Date());
			}
		});
	}
	
	public void executeHoroscopeSagittariusSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.SAGITTARIUS, new Date());
			}
		});
	}
	
	public void executeHoroscopeScorpioSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.SCORPIO, new Date());
			}
		});
	}
	
	public void executeHoroscopeTaurusSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.TAURUS, new Date());
			}
		});
	}
	
	public void executeHoroscopeVirgoSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendHoroscope(Horoscope.VIRGO, new Date());
			}
		});
	}
	
	/**
	 * SOAPIES
	 */
	public void executeSoapieSend() {
		executeSoapieGenerationsSend();
		executeSoapieIsidingoSend();
		executeSoapie7deLaanSend();
	}
	
	public void executeSoapieGenerationsSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendSoapie(Soapie.GENERATIONS, new Date());
			}
		});
	}
	
	public void executeSoapieIsidingoSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendSoapie(Soapie.ISIDINGO, new Date());
			}
		});
	}
	
	public void executeSoapie7deLaanSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendSoapie(Soapie.SEWENDELAAN, new Date());
			}
		});
	}
	
	/**
	 * SPORT
	 */
	public void executeEPLSoccerSend(final int id) {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendSport(Sport.EPL, id);
			}
		});
	}
	
	public void executePSLSoccerSend(final int id) {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendSport(Sport.PSL, id);
			}
		});
	}
	
	/**
	 * WEATHER
	 */	
	public void executeWeatherSend() {
		executeWeatherBloemfonteinSend();
		executeWeatherCapeTownSend();
		executeWeatherDurbanSend();
		executeWeatherJohannesburgSend();
		executeWeatherPortElizabethSend();
		executeWeatherPretoriaSend();
	}
	
	public void executeWeatherBloemfonteinSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendWeather(Weather.BLOEMFONTEIN, new Date());
			}
		});
	}
	
	public void executeWeatherCapeTownSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendWeather(Weather.CAPE_TOWN, new Date());
			}
		});
	}
	
	public void executeWeatherDurbanSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendWeather(Weather.DURBAN, new Date());
			}
		});
	}
	
	public void executeWeatherJohannesburgSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendWeather(Weather.JOHANNESBURG, new Date());
			}
		});
	}
	
	public void executeWeatherPortElizabethSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendWeather(Weather.PORT_ELIZABETH, new Date());
			}
		});
	}
	
	public void executeWeatherPretoriaSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendWeather(Weather.PRETORIA, new Date());
			}
		});
	}
	
	
	/**
	 * ZANGOMA
	 */
	
	public void executeZangomaSend() {
		executeZangomaEnglishSend();
		executeZangomaZuluSend();
	}
	
	public void executeZangomaEnglishSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendZangoma(Zangoma.ENGLISH, new Date());
			}
		});
	}
	
	public void executeZangomaZuluSend() {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				ContentComposerManager.sendZangoma(Zangoma.ZULU, new Date());
			}
		});
	}

}
