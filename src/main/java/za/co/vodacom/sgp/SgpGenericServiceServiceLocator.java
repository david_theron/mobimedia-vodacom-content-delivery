/**
 * SgpGenericServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package za.co.vodacom.sgp;

import com.mobimedia.vodacom.content.delivery.config.AppConfig;

public class SgpGenericServiceServiceLocator extends org.apache.axis.client.Service implements za.co.vodacom.sgp.SgpGenericServiceService {

    public SgpGenericServiceServiceLocator() {
    }


    public SgpGenericServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SgpGenericServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GenericService
    private java.lang.String GenericService_address = AppConfig.getInstance().getFusionEndpoint();

    public java.lang.String getGenericServiceAddress() {
        return GenericService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GenericServiceWSDDServiceName = "GenericService";

    public java.lang.String getGenericServiceWSDDServiceName() {
        return GenericServiceWSDDServiceName;
    }

    public void setGenericServiceWSDDServiceName(java.lang.String name) {
        GenericServiceWSDDServiceName = name;
    }

    public za.co.vodacom.sgp.SgpGenericService getGenericService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GenericService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGenericService(endpoint);
    }

    public za.co.vodacom.sgp.SgpGenericService getGenericService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            za.co.vodacom.sgp.GenericServiceSoapBindingStub _stub = new za.co.vodacom.sgp.GenericServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getGenericServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGenericServiceEndpointAddress(java.lang.String address) {
        GenericService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (za.co.vodacom.sgp.SgpGenericService.class.isAssignableFrom(serviceEndpointInterface)) {
                za.co.vodacom.sgp.GenericServiceSoapBindingStub _stub = new za.co.vodacom.sgp.GenericServiceSoapBindingStub(new java.net.URL(GenericService_address), this);
                _stub.setPortName(getGenericServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GenericService".equals(inputPortName)) {
            return getGenericService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://sgp.vodacom.co.za", "SgpGenericServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://sgp.vodacom.co.za", "GenericService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GenericService".equals(portName)) {
            setGenericServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
