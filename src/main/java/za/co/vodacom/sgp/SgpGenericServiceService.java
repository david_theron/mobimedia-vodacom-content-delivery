/**
 * SgpGenericServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package za.co.vodacom.sgp;

public interface SgpGenericServiceService extends javax.xml.rpc.Service {
    public java.lang.String getGenericServiceAddress();

    public za.co.vodacom.sgp.SgpGenericService getGenericService() throws javax.xml.rpc.ServiceException;

    public za.co.vodacom.sgp.SgpGenericService getGenericService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
