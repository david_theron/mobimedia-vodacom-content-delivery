/**
 * SgpGenericService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package za.co.vodacom.sgp;

public interface SgpGenericService extends java.rmi.Remote {
    public java.lang.String[] getActions(java.lang.String service) throws java.rmi.RemoteException, za.co.vodacom.sgp.SgpGenericException;
    public za.co.vodacom.sgp.NVPair[] execute(java.lang.String serviceName, java.lang.String action, java.lang.String transactionId, za.co.vodacom.sgp.NVPair[] parameters) throws java.rmi.RemoteException, za.co.vodacom.sgp.SgpGenericException;
    public java.lang.String getTransactionId(java.lang.String name) throws java.rmi.RemoteException;
    public java.lang.String[] getServices() throws java.rmi.RemoteException, za.co.vodacom.sgp.SgpGenericException;
    public za.co.vodacom.sgp.NVPair[] callInternal(java.lang.String username, java.lang.String serviceName, java.lang.String action, java.lang.String transactionId, za.co.vodacom.sgp.NVPair[] parameters) throws java.rmi.RemoteException, za.co.vodacom.sgp.SgpGenericException;
}
